package co.id.adira.mongodb.model;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

@Document(collection = "RoadType")
public class RoadType {
@Id
private String typeId;
private String typeName;
private Date effectiveDate;
private String active;
private String createdBy;
private Date createDate;
private String updateBy;
private Date   updateDate;

public RoadType(String typeName, Date effectiveDate , String active, String createdBy, Date createDate, String updateBy, Date   updateDate) {
	
	super();
	this.typeName = typeName;
	this.effectiveDate = effectiveDate;
	this.active =  active;
	this.createdBy = createdBy;
	this.createDate = createDate;
	this.updateBy = updateBy;
	this.updateDate = updateDate;
}

}
