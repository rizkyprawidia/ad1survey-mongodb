package co.id.adira.mongodb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ad1surveyMongodbApplication {

	public static void main(String[] args) {
		SpringApplication.run(Ad1surveyMongodbApplication.class, args);
	}

}
