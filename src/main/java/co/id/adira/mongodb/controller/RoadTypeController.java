package co.id.adira.mongodb.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import co.id.adira.mongodb.model.RoadType;
import co.id.adira.mongodb.repository.RoadTypeRepository;

@RestController
public class RoadTypeController {
	@Autowired
	private RoadTypeRepository roadTypeRepository;
	
	
	
	@GetMapping("/findAllRoadType")
	public List<RoadType> getroadTypes(){
		return roadTypeRepository.findAll();
	}
	
	@PostMapping("/insertRoadType")
	public String saveRoadType(@RequestBody RoadType roadType) {
		RoadType insert = roadTypeRepository.insert(roadType);
		return "Added Road Type with id : " + roadType.getTypeId();
		
	}
	
}
