package co.id.adira.mongodb.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import co.id.adira.mongodb.model.RoadType;

public interface RoadTypeRepository extends MongoRepository<RoadType, String>{

}
